// JAVASCRIPT - ARRAY MANIPULATIONS

// mutated or iterated - pwede ibalik.

// Array Methods - Javascript has built-in functions and methods for arrays. This allows us to manipulate and access array items.
/*--------------------------------------------------------------*/

// I. A.
// Mutator Methods

/*
	- Mutator methods are functions that "mutate" or change an array after they're created.
	- These methods manipulate te original array performing various task such as adding and removing elements.

*/


// push()

/*
	- Adds an element in the end of an array And return the array's length.

	Syntax:
		arrayName.push();

*/

// hindi ito .length
// isa lang itong variable ang fruitsLength
// Q: San sya mostly nagamit sir ?
// Ans: Mag add tayo ng information or array. Doon siya magagamit.
// () means methods
// best practice rin ito sa Mongo DB

// for example:
let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

console.log('Current array:');
console.log(fruits);


let fruitsLength = fruits.push('Mango');
// console.log(fruitsLength);
console.log('Mutated array from push method:')
console.log(fruits)

/*---------------------------------------------------------------*/
// I.B.

// Adding multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log('Mutated array from push method:')
console.log(fruits);

/*--------------------------------------------------------------*/
// II.
// pop()
/*
	- REMOVES THE LAST ELEMENT in an Array AND returns the removed element.
	- Syntax
		arrayName.pop();

*/

// for example:
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log('Mutated array from pop method:');
console.log(fruits);

// So, the element to removed is 'Guava'

/*---------------------------------------------------------------*/

// III.
// unshift()
/*
	- ADDS ONE OR MORE ELEMENTS at the BEGINNING of an array.

	Syntax:

		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB');
*/

// You can all add element inside of single quotation mark.


// for example:
fruits.unshift('Lime','Banana');
console.log('Mutated array from unshift method:');
console.log(fruits);

// the 'Lime','Banana' is added in the beginning of array.

/*---------------------------------------------------------------*/
// IV.
// We can read this as Shift method
// Shift()
/*
	- REMOVES an ELEMENT AT THE BEGINNING of an Array and Returns the removed element.

	Syntax:
		arrayName.shift();
*/

// for example:
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log('Mutated array from shift method');
console.log(fruits);


// Q: dba sir may pangkalahatan si splice() tapos select index para iwas risk.
// Ans: YES
/*---------------------------------------------------------------*/

// V.
// splice()

// Splice is a mutator and slice is hindi.

/*
	- Simultaneously removes elements from a specified index number and adds element

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

*/

// for example:
fruits.splice(1, 2, 'Lime', 'Cherry');
console.log('Mutated array from splice method:');
console.log(fruits);

// inside of () is what we called index.
// We can also add string such as  'Lime', 'Cherry' inside if ()
// nasubstitute ung value nung index 1 & 2
// 1 is Apple and 2 is Orange
// So 1 & 2 is na substitute at nalagyan ng string na 'Lime', 'Cherry'
// Q: sir sa splice pwedeng remove lang dina mag Add or need tlga laging may add kay splice???
// Ans: Yes, kukunin mo lang yung string na 'Lime', 'Cherry'

// pwede gumamit ng 0 kung mag add or delete

/*-----------------------------------------------------------*/

// VI.
// sort()
/*
	- Rearrange the array elements in alphanumeric order.

	Syntax:
		arrayName.sort();

*/

// for example:
const randomThings = ['']

fruits.sort();
console.log('Mutated array from sort method:');
console.log(fruits);

// in sort() it always priorities the Capital letter/Upper case letters before the small letter in a alphabetical order.
// in assey codes has a binary. yan yung utf-8. sa utf=8 dyan siya bumabasi kapag nag,ssort.

/*--------------------------------------------------------------*/

// VII.
// reverse()

/*	
	-Reverse the order of array elements
	-Syntax
		arrayName.reverse();
*/

randomThings.reverse();
console.log('Mutated array from reverse method:');
console.log(randomThings);

/*---------------------------------------------------------------*/

// VIII.
// Non-Mutator Methods
/*
	- Non-Mutator methods are functions  that do not modify or change an array after they're created.
	- These method do not manipulate the original array performing various  tasks such as returning elements from an array and combining array and printing output.
	
*/
// We access our array but we cannot manipulate and the non-mutators ay gine,get lang.

/*---------------------------------------------------------------*/

// IV.
// indexOf()
/*
	- Returns the index number of the first matching element found in an array.
	- If no match was found, the result will be -1
	- The search process will be done from first element preoceeding to the element.


	Syntax:
		arrayName.indexOf(searchValue)

*/

// for example:
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'BR', 'FR', 'DE']

let firstIndex  = countries.indexOf('CAN');
console.log('Result of indexOf method:' + firstIndex);

// let us find the 'CAN' string then save it and go to your browser to see the results.
// Binibilang niya ang position na 'CAN'

// lastIndexOf() is may bug siya. Ginagamit siya kapag may denedelete na specific.
// lastindexof is pag may duplicate sa array mo... i re return nya ung last index instead of first match. Pag wala duplicate ung first index pa rin ilalabas nya.



/*---------------------*/


// X.
// slice()
/*
	- Portion/slices elements from an array AND returns a new array.

	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex endingIndex);

*/
// Slicing off elements from a specified index to the first element.


// for example:
let slicedArrayA = countries.slice(4);
console.log('result from sliceA method');
console.log(countries);
console.log(slicedArrayA);

// hindi na mutate yung original or hindi nabago yung original kahit ilagay sa ibaba ng console.log(sliceArrayA); ang console.log(countries);
// nag,modified lang tayo dito sa slice.
// Q: kumbaga nag call ka lang sir sa mismong array? ans: YES
// sa dulo siya nag slice such as 'TH', 'BR', 'FR', 'DE'



/*---------------------*/
// if gusto ko sa hulihan?
// Slicing off elements starting from the last element of an array.

// Slicing off elements from a specified index to another index

// Note: the last element is not included.


// for example:
let slicedArrayB = countries.slice(2,4);
console.log('Result from sliceB method:');
console.log(slicedArrayB)

// The count starts at 0 then stops at 4 then hindi esasama.
// The 'TH' is the middle. 
// 'US', 'PH', 'CAN', 'SG', 'TH', 0 is the 'US' then it will stops the count in 'TH' because 'TH' is the number 4

// Slicing off elements starting from the last element of an array


/*------------------------*/
// for example:
let slicedArrayC = countries.slice(-3);
console.log('Result from sliceC method:');
console.log(slicedArrayC);

// Slicing off 

// We put -3 inside of the () kung gusto natin kukuhanin yung 'BR', 'FR', 'DE'
// huling tatlo lang nag return.
// Kung sino naka,una sa array such as 'BR', 'FR', 'DE' siya po ang unang kukuhain.

/*-------------------------*/

// XI
// forEach();

/*
	- Similar to a for loop that iterates on each array element.
	- For each item in the array, the anonymous function passed in forEach() method will be run.
	- arrayName - plural for good practice (e.i. countries).
	-parameter - singular (e.i. country).
	- Note: it must have function.

	Syntax:
		arrayName.forEach(function(indivElement){
			statement
		})

*/

// for example:
countries.forEach(function(country){

	console.log(country);

})



/*countries.forEach(function(country){
	
	counsole.log()
})
*/

// Ginagawa is kinukuha niya lahat ng arrays. e,pprint niya lahat or query at naka anonimous function.
// ma ppass siya.
// ang good practices nito is yung countries dapat plural sa array name. tapos sa loob ng () is singular na kagaya ng country.
// pwede e,invoke lang sa function kc naka,parameter yung country.


/*---------------------------------------------------------*/
// XII
// includes()
/*
	includes()

	- includes() method checks if the argument passed can be found in the array.
	- Boleans means yung true or false.
	- it returns a boolean which can be saved in a variable.
		- return true if the argument is found in the array.
		- return false if it is not.

		Syntax:
			arrayName.includes(<argumentToFind>)

*/

// for example:

 // products of array

let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = products.includes("Mouse");

console.log(productFound1);//returns true

let productFound2 = products.includes("Headset");

console.log(productFound2);//returns false

/*-----------------------------------------------------------*/


// ACTIVITY:
// .pop() kapag nag ddelete ng friend.
